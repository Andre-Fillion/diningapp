package a2;


import java.util.*;

/**
 * Collection of categories and a list of items (recipes/food name) in the category
 */
public class MealMenu {

  private Map<String, List<String>> categories = new HashMap<>();

  /**
   * Default constructor, required by JSON deserializer
   */
  public MealMenu() {
  }

  public Map<String, List<String>> getCategories() {
    return categories;
  }

  @Override
  public String toString() {
    return "MealMenu{" +
        "categories=" + categories +
        '}';
  }
}
