package a2;

import java.util.*;

/**
 * A collection of meals (breakfast, lunch, dinner, late night) for a particular day
 */
public class DayMenu {

  private String url; // menu URL
  private Map<String, MealMenu> meals;

  /**
   * Default constructor, needed by Jackson deserializer
   */
  public DayMenu() {
  }

  public String getUrl() {
    return url;
  }

  public Map<String, MealMenu> getMeals() {
    return meals;
  }

  @Override
  public String toString() {
    return "DayMenu{" +
        "url='" + url + '\'' +
        ", meals=" + meals +
        '}';
  }
}
