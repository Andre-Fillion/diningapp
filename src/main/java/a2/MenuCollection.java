package a2;

import java.util.HashMap;

/**
 * Collection of day dayMenus from multiple days from a particular dining location Each DayMenu can
 * be retrieved using the calendar date in the format of YYYY-MM-DD
 */
public class MenuCollection {

  HashMap<String, DayMenu> dayMenus = new HashMap<>(); // map a date DD/MM/YYYY to a day menu

  /**
   * Default constructor, required by JSON deserializer
   */
  public MenuCollection() {
  }

  public HashMap<String, DayMenu> getDayMenus() {
    return dayMenus;
  }

  @Override
  public String toString() {
    return "MenuCollection{" +
        "dayMenus=" + dayMenus +
        '}';
  }
}
