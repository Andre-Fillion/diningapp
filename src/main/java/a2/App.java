package a2;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.*;
import com.fasterxml.jackson.databind.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Scanner;


public class App {
    public static void main(String[] args) throws IOException {
        System.out.println("Enter A Number 1-4\n 1) List all locations\n 2) List dates for Tim and Jeanne's\n 3) List available salads\n 4) List menu items\n");
        Scanner scan = new Scanner(System.in);
        int selection = scan.nextInt();

        switch (selection) {
            case 1:
                System.out.println("Listing Locations...\n");
                try {
                    listLocations();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                System.out.println("Listing Dates...\n");
                try {
                    listDates();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case 3:
                System.out.println("Listing Salads Available From Tim & Jeanne's Dining Commons On 2/2/2020...");
                listSalads();
                break;
            case 4:
                System.out.println("Listing All Menu Items...");
                listItems();
                break;
            default:
                System.out.println("Invalid input");
        }
    }


    //Name And Description
    public static List<Location> listLocations() throws IOException {
        int counter = 0;
        URL jsonFile = new URL("https://qwgdxhk657.execute-api.us-east-1.amazonaws.com/dev/menus");
        ObjectMapper mapper = new ObjectMapper();
        JsonFactory factory = mapper.getFactory();
        JsonParser parser = factory.createParser(jsonFile);
        List<Location> locations = mapper.readValue(jsonFile,
                new TypeReference<List<Location>>() {
                });
        while (counter < 6) {
            System.out.println(locations.get(counter).getName() + "\nDescription: " + locations.get(counter).getDescription() + "\n");
            counter++;
        }
        return locations;
    }

    //All Available Day Menus for Tim and Jeanne's
    public static List<Location> listDates() throws IOException {
        File jsonFile = new File("src\\main\\java\\a2\\menus.json");
        ObjectMapper mapper = new ObjectMapper();
        JsonFactory factory = mapper.getFactory();
        JsonParser parser = factory.createParser(jsonFile);
        List<Location> dates = mapper.readValue(jsonFile,
                new TypeReference<List<Location>>() {
                });
        System.out.println("All Dates Tim and Jeanne's Menu Is Available:\n");
        String trimmedDate = dates.get(0).getMenuCollection().toString();
        String[] part1 = trimmedDate.split("=DayMenu");
        String date2 = part1[1].toString();
        String date1 = part1[0];
        System.out.println(" " + date1.substring(25, 34));
        System.out.println(date2.substring(date2.length() - 10, date2.length()));
        String[] part2 = trimmedDate.split("Raisin BreadÂ ]}}}},");
        String date3 = part2[1].toString();
        System.out.println(date3.substring(0, 10));
        String[] part3 = date3.split("Pesto BreadÂ ]}}}},");
        String date4 = part3[1];
        System.out.println(date4.substring(0, 10));
        ;
        String[] part4 = date4.split("ChallahÂ ]}}}},");
        String date5 = part4[1];
        System.out.println(date5.substring(0, 10));
        String[] part5 = date5.split("Multiseed Rye BreadÂ ]}}}},");
        String date6 = part5[1];
        System.out.println(date6.substring(0, 9));
        String[] part6 = date6.split("Semolina BreadÂ ]}}}},");
        String date7 = part6[1];
        System.out.println(date7.substring(0, 10));
        return dates;
    }

    //Tim and Jeanne's Salads on 2/2/2020
    public static List<Location> listSalads() throws IOException {
        File jsonFile = new File("src\\main\\java\\a2\\menus.json");
        ObjectMapper mapper = new ObjectMapper();
        JsonFactory factory = mapper.getFactory();
        JsonParser parser = factory.createParser(jsonFile);
        List<Location> salads = mapper.readValue(jsonFile,
                new TypeReference<List<Location>>() {
                });
        String collection = salads.get(0).getMenuCollection().toString();
        String[] iniTrim = collection.split("2/2");
        String salad = iniTrim[1].toString();
        String[] finTrim = salad.split("Composed Salads=");
        System.out.println(finTrim[1].toString().substring(1, 17));
        return salads;
    }

    //Wednesday 1/29/2020 at Tim and Jeanne's Dining Commons
    public static List<Location> listItems() throws IOException {
        File jsonFile = new File("src\\main\\java\\a2\\menus.json");
        ObjectMapper mapper = new ObjectMapper();
        JsonFactory factory = mapper.getFactory();
        JsonParser parser = factory.createParser(jsonFile);
        List<Location> items = mapper.readValue(jsonFile,
                new TypeReference<List<Location>>() {
                });
        String collection = items.get(0).getMenuCollection().toString();
        String[] part1 = collection.split("Focaccia BreadÂ ]}}}},");
        String wedMenu = part1[1];
        System.out.println(wedMenu);
        String[] part2 = wedMenu.split("Specialty=");
        String Dinner = part2[1];
        System.out.println("**Specialty\n"+Dinner.substring(1,18)+"\n**Clean Eats\n"+Dinner.substring(35,46)+"\n"+Dinner.substring(50,60)+"\n"+Dinner.substring(64,95)+"\n"+Dinner.substring(99,132)+"\n"+Dinner.substring(136,155));
        System.out.println("**Chef's Table\n"+Dinner.substring(174,196)+"\n"+Dinner.substring(200,223));
        System.out.println("**Pasta Bar\n"+Dinner.substring(239,252)+"\n"+Dinner.substring(256,270)+"\n"+Dinner.substring(274,284)+"\n"+Dinner.substring(288,305));
        System.out.println("**Desserts\n"+Dinner.substring(320,335)+"\n"+Dinner.substring(339,358));
        System.out.println("**Sides\n"+Dinner.substring(370,393)+"\n"+Dinner.substring(397,423)+"\n"+Dinner.substring(427,439)+"\n"+Dinner.substring(443,462)+"\n"+Dinner.substring(466,476));
        return items;
    }

}
