package a2;

/**
 * Dining Location
 */
public class Location {

  // URL prefix for all of the
  public static final String URL_PREFIX = "http://www.westfield.ma.edu/diningservices/foodpro.net/";

  private String name;
  private String description;
  private String url;
  private MenuCollection menuCollection;

  /**
   * Default constructor, required by JSON deserializer
   */
  public Location() {
  }

  public MenuCollection getMenuCollection() {
    return menuCollection;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @Override
  public String toString() {
    return "Location{" +
        "name='" + name + '\'' +
        ", description='" + description + '\'' +
        ", url='" + url + '\'' +
        ", dayMenus=" + menuCollection +
        '}';
  }
}
